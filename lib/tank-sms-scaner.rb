require 'sequel'
require 'awesome_print'
require 'logger'

$logger = Logger.new('tanksms.log','daily')

begin
db = Sequel.ado(conn_string:'Provider:Microsoft.Jet.OLEDB.4.0;Data Source="C:\Program Files (x86)\Kcell CONNECT\db.dat"')
ap db

sms_table = db[:sms]

last_sms = sms_table.order(:id).last

ap last_sms
rescue Exception => ex
	ap ex.message
	$logger.error ex.message
	$logger.info $@
end	
