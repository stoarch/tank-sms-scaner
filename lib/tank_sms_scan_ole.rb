# encoding: IBM866
require 'win32ole'
require 'awesome_print'
require 'net/http'
require 'date'

class AccessDb
    attr_accessor :mdb, :connection, :data, :fields

    def initialize(mdb=nil)
        @mdb = mdb
        @connection = nil
        @data = nil
        @fields = nil
    end

    def open
        connection_string =  'Provider=Microsoft.Jet.OLEDB.4.0;Data Source='
        connection_string << @mdb
        @connection = WIN32OLE.new('ADODB.Connection')
        @connection.Open(connection_string)
    end

    def query(sql)
        recordset = WIN32OLE.new('ADODB.Recordset')
        recordset.Open(sql, @connection)
        @fields = []
        recordset.Fields.each do |field|
            @fields << field.Name
        end
        begin
            @data = recordset.GetRows.transpose
        rescue
            @data = []
        end
        recordset.Close
    end

    def execute(sql)
        @connection.Execute(sql)
    end

    def close
        @connection.Close
    end
end

=begin
[
    [ 0] "ID",
    [ 1] "Smsindex",
    [ 2] "Sender",
    [ 3] "Receiver",
    [ 4] "sendername",
    [ 5] "SendTime",
    [ 6] "RecvTime",
    [ 7] "CodeType",
    [ 8] "Content",
    [ 9] "Status",
    [10] "SMSCentre",
    [11] "Location",
    [12] "Reserved",
    [13] "IsConcat",
    [14] "ConcatRefIndex",
    [15] "ConcatTotalMount",
    [16] "ConcatSeq"
]
=end

ID = 0
Sender = 2
Content = 8

class SmsMessage
	attr_accessor :id, :sender, :content

	def initialize( sms_data )
		@id = sms_data[ID]
		@sender = sms_data[Sender]
		@content = sms_data[Content]
	end
end

TANK1_CELL_NO = '+77751997612'
TANK2_CELL_NO = '+77471299209'
TEST_PHONE = '+77758235687'

TANK1 = 1
TANK2 = 2

$uri = URI.parse 'http://192.168.10.8:4567/tank_param'

def send_command( tank_no, command, value = 0 )
	Net::HTTP.post_form( $uri, {"code" => tank_no, "command" => command, "value" => value } )
end

LEVEL_COMMAND = 1
OVERFLOW_COMMAND = 2
EMPTY_COMMAND = 3
SENSOR_FAILURE_COMMAND = 10

def send_normal_level( tank_no )
	send_command( tank_no, LEVEL_COMMAND, 5 )
end

def send_overflow_level( tank_no )
	send_command( tank_no, OVERFLOW_COMMAND )
end

def send_empty_level( tank_no )
	send_command( tank_no, EMPTY_COMMAND)
end

class String
	def valid_float?
		!!Float(self) rescue false
	end
end

$tank1_id_sms_id = 0
$tank2_id_sms_id = 0


# MAIN PROGRAM #

	ec1 = Encoding::Converter.new "UTF-8","IBM866",:invalid=>:replace,:undef=>:replace,:replace=>""
	ec2 = Encoding::Converter.new "IBM866","UTF-8",:invalid=>:replace,:undef=>:replace,:replace=>""

print "Query sms.."
loop do
	print '.'

	db = AccessDb.new('"C:\Program Files (x86)\Kcell CONNECT\db.dat"')
	db.open

	db.query("Select top 1 * from sms order by id desc")
	field_names = db.fields
	rows = db.data

	sms_msg = SmsMessage.new( rows[0] )

	if sms_msg.sender == TANK1_CELL_NO or  sms_msg.sender == TEST_PHONE #or  sms_msg.sender == TEST_PHONE
		if( sms_msg.id > $tank1_id_sms_id )
			$tank1_id_sms_id = sms_msg.id

			p "TANK 1: Handling:: #{DateTime.now}"
			ap sms_msg

			if sms_msg.content.rstrip == "���孨� �஢���" 
					p "Normal level"
					send_normal_level(TANK1)
			elsif sms_msg.content.rstrip == "���਩�� �஢��"
					p "Overflow"
					send_overflow_level(TANK1)

			elsif sms_msg.content.rstrip == "������ �஢���" 
					p "Underflow"
					send_empty_level(TANK1)
			end
		end
	elsif sms_msg.sender == TANK2_CELL_NO 

		if( sms_msg.id > $tank2_id_sms_id )
			$tank2_id_sms_id = sms_msg.id

			p "TANK 2: Handling #{DateTime.now}"
			ap sms_msg

			if ( sms_msg.content.valid_float? ) 
					p "Level"
					send_command(TANK2, LEVEL_COMMAND, (sms_msg.content.to_f*100).round(0)) 
			elsif sms_msg.content == "overflow" 
					p "Overflow"
					send_overflow_level(TANK2)

			elsif sms_msg.content == "obriv sensor" 
					p "Sensor failure"
					send_command(TANK2, SENSOR_FAILURE_COMMAND )
			end
		end
	end
	db.close

	sleep(5)#sec

end
